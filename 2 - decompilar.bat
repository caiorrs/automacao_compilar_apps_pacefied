@echo off

:: DECOMPILANDO TUDO

echo decompilando AdvertiseManager
java -jar c:\Windows\apktool.jar d -f -s -o App_Extraido\AdvertiseManager App_Orig\AdvertiseManager.apk
echo decompilando Alipay
java -jar c:\Windows\apktool.jar d -f -s -o App_Extraido\Alipay App_Orig\Alipay.apk

echo decompilando AmazfitWeather
java -jar c:\Windows\apktool.jar d -f -s -o App_Extraido\AmazfitWeather App_Orig\AmazfitWeather.apk

echo decompilando Bluetooth
java -jar c:\Windows\apktool.jar d -f -s -o App_Extraido\Bluetooth App_Orig\Bluetooth.apk

echo decompilando CaptivePortalLogin
java -jar c:\Windows\apktool.jar d -f -s -o App_Extraido\CaptivePortalLogin App_Orig\CaptivePortalLogin.apk

echo decompilando CertInstaller
java -jar c:\Windows\apktool.jar d -f -s -o App_Extraido\CertInstaller App_Orig\CertInstaller.apk

echo decompilando ChargingUI
java -jar c:\Windows\apktool.jar d -f -s -o App_Extraido\ChargingUI App_Orig\ChargingUI.apk

echo decompilando HmLab
java -jar c:\Windows\apktool.jar d -f -s -o App_Extraido\HmLab App_Orig\HmLab.apk

echo decompilando HmMediaPlayer
java -jar c:\Windows\apktool.jar d -f -s -o App_Extraido\HmMediaPlayer App_Orig\HmMediaPlayer.apk

echo decompilando HmTvHelper
java -jar c:\Windows\apktool.jar d -f -s -o App_Extraido\HmTvHelper App_Orig\HmTvHelper.apk

echo decompilando HmVoice
java -jar c:\Windows\apktool.jar d -f -s -o App_Extraido\HmVoice App_Orig\HmVoice.apk

echo decompilando HuamiIME
java -jar c:\Windows\apktool.jar d -f -s -o App_Extraido\HuamiIME App_Orig\HuamiIME.apk

echo decompilando HuamiSelfTest
java -jar c:\Windows\apktool.jar d -f -s -o App_Extraido\HuamiSelfTest App_Orig\HuamiSelfTest.apk

echo decompilando HuamiWatchFaces
java -jar c:\Windows\apktool.jar d -f -s -o App_Extraido\HuamiWatchFaces App_Orig\HuamiWatchFaces.apk

echo decompilando KeyChain
java -jar c:\Windows\apktool.jar d -f -s -o App_Extraido\KeyChain App_Orig\KeyChain.apk

echo decompilando MyWatch
java -jar c:\Windows\apktool.jar d -f -s -o App_Extraido\MyWatch App_Orig\MyWatch.apk

echo decompilando PackageInstaller
java -jar c:\Windows\apktool.jar d -f -s -o App_Extraido\PackageInstaller App_Orig\PackageInstaller.apk

echo decompilando SensorList
java -jar c:\Windows\apktool.jar d -f -s -o App_Extraido\SensorList App_Orig\SensorList.apk

echo decompilando SetupWizard
java -jar c:\Windows\apktool.jar d -f -s -o App_Extraido\SetupWizard App_Orig\SetupWizard.apk

echo decompilando TrainingPlan
java -jar c:\Windows\apktool.jar d -f -s -o App_Extraido\TrainingPlan App_Orig\TrainingPlan.apk

echo decompilando UnlockMIUI
java -jar c:\Windows\apktool.jar d -f -s -o App_Extraido\UnlockMIUI App_Orig\UnlockMIUI.apk

echo decompilando WearBLE
java -jar c:\Windows\apktool.jar d -f -s -o App_Extraido\WearBLE App_Orig\WearBLE.apk

echo decompilando WearCompass
java -jar c:\Windows\apktool.jar d -f -s -o App_Extraido\WearCompass App_Orig\WearCompass.apk

echo decompilando WearHealth
java -jar c:\Windows\apktool.jar d -f -s -o App_Extraido\WearHealth App_Orig\WearHealth.apk

echo decompilando WearHeartRate
java -jar c:\Windows\apktool.jar d -f -s -o App_Extraido\WearHeartRate App_Orig\WearHeartRate.apk

echo decompilando WearLogger
java -jar c:\Windows\apktool.jar d -f -s -o App_Extraido\WearLogger App_Orig\WearLogger.apk

echo decompilando WearSensorService
java -jar c:\Windows\apktool.jar d -f -s -o App_Extraido\WearSensorService App_Orig\WearSensorService.apk

echo decompilando WearServices
java -jar c:\Windows\apktool.jar d -f -s -o App_Extraido\WearServices App_Orig\WearServices.apk

echo decompilando WearSmartHome
java -jar c:\Windows\apktool.jar d -f -s -o App_Extraido\WearSmartHome App_Orig\WearSmartHome.apk

echo decompilando WearSports
java -jar c:\Windows\apktool.jar d -f -s -o App_Extraido\WearSports App_Orig\WearSports.apk

echo decompilando XimalayaSound
java -jar c:\Windows\apktool.jar d -f -s -o App_Extraido\XimalayaSound App_Orig\XimalayaSound.apk

echo decompilando webview
java -jar c:\Windows\apktool.jar d -f -s -o App_Extraido\webview App_Orig\webview.apk


echo.
echo.

echo decompilando BackupRestoreConfirmation
java -jar c:\Windows\apktool.jar d -f -s -o App_Extraido\BackupRestoreConfirmation App_Orig\BackupRestoreConfirmation.apk

echo decompilando DefaultContainerService
java -jar c:\Windows\apktool.jar d -f -s -o App_Extraido\DefaultContainerService App_Orig\DefaultContainerService.apk

echo decompilando ExternalStorageProvider
java -jar c:\Windows\apktool.jar d -f -s -o App_Extraido\ExternalStorageProvider App_Orig\ExternalStorageProvider.apk

echo decompilando FusedLocation
java -jar c:\Windows\apktool.jar d -f -s -o App_Extraido\FusedLocation App_Orig\FusedLocation.apk

echo decompilando HmAlarmClock
java -jar c:\Windows\apktool.jar d -f -s -o App_Extraido\HmAlarmClock App_Orig\HmAlarmClock.apk

echo decompilando InputDevices
java -jar c:\Windows\apktool.jar d -f -s -o App_Extraido\InputDevices App_Orig\InputDevices.apk

echo decompilando ManagedProvisioning
java -jar c:\Windows\apktool.jar d -f -s -o App_Extraido\ManagedProvisioning App_Orig\ManagedProvisioning.apk

echo decompilando MediaProvider
java -jar c:\Windows\apktool.jar d -f -s -o App_Extraido\MediaProvider App_Orig\MediaProvider.apk

echo decompilando OtaWatch
java -jar c:\Windows\apktool.jar d -f -s -o App_Extraido\OtaWatch App_Orig\OtaWatch.apk

::echo decompilando ProxyHandler
::java -jar c:\Windows\apktool.jar d -f -s -o App_Extraido\ProxyHandler App_Orig\ProxyHandler.apk

echo decompilando SettingsProvider
java -jar c:\Windows\apktool.jar d -f -s -o App_Extraido\SettingsProvider App_Orig\SettingsProvider.apk

echo decompilando SharedStorageBackup
java -jar c:\Windows\apktool.jar d -f -s -o App_Extraido\SharedStorageBackup App_Orig\SharedStorageBackup.apk

echo decompilando Shell
java -jar c:\Windows\apktool.jar d -f -s -o App_Extraido\Shell App_Orig\Shell.apk

echo decompilando SystemUI
java -jar c:\Windows\apktool.jar d -f -s -o App_Extraido\SystemUI App_Orig\SystemUI.apk

echo decompilando WearAirPlaneMode
java -jar c:\Windows\apktool.jar d -f -s -o App_Extraido\WearAirPlaneMode App_Orig\WearAirPlaneMode.apk

echo decompilando WearLauncher
java -jar c:\Windows\apktool.jar d -f -s -o App_Extraido\WearLauncher App_Orig\WearLauncher.apk

echo decompilando WearSettings
java -jar c:\Windows\apktool.jar d -f -s -o App_Extraido\WearSettings App_Orig\WearSettings.apk

echo decompilando WifiUploadData
java -jar c:\Windows\apktool.jar d -f -s -o App_Extraido\WifiUploadData App_Orig\WifiUploadData.apk


echo.
echo.

echo acabou de decompilar, os arquivos estao no diretorio App_Extraido
pause

exit