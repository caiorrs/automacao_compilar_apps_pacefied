@echo off

:: compilando TUDO

cd App_Extraido

echo compilando AdvertiseManager
java -jar c:\Windows\apktool.jar b -c AdvertiseManager
echo compilando Alipay
java -jar c:\Windows\apktool.jar b -c Alipay
echo compilando AmazfitWeather
java -jar c:\Windows\apktool.jar b -c AmazfitWeather 
echo compilando Bluetooth
java -jar c:\Windows\apktool.jar b -c Bluetooth 
echo compilando CaptivePortalLogin
java -jar c:\Windows\apktool.jar b -c CaptivePortalLogin 
echo compilando CertInstaller
java -jar c:\Windows\apktool.jar b -c CertInstaller 
echo compilando ChargingUI
java -jar c:\Windows\apktool.jar b -c ChargingUI 
echo compilando HmLab
java -jar c:\Windows\apktool.jar b -c HmLab 
echo compilando HmMediaPlayer
java -jar c:\Windows\apktool.jar b -c HmMediaPlayer 
echo compilando HmTvHelper
java -jar c:\Windows\apktool.jar b -c HmTvHelper 
echo compilando HmVoice
java -jar c:\Windows\apktool.jar b -c HmVoice 
echo compilando HuamiIME
java -jar c:\Windows\apktool.jar b -c HuamiIME 
echo compilando HuamiSelfTest
java -jar c:\Windows\apktool.jar b -c HuamiSelfTest 
echo compilando HuamiWatchFaces
java -jar c:\Windows\apktool.jar b -c HuamiWatchFaces 
echo compilando KeyChain
java -jar c:\Windows\apktool.jar b -c KeyChain 
echo compilando MyWatch
java -jar c:\Windows\apktool.jar b -c MyWatch 
echo compilando PackageInstaller
java -jar c:\Windows\apktool.jar b -c PackageInstaller 
echo compilando SensorList
java -jar c:\Windows\apktool.jar b -c SensorList 
echo compilando SetupWizard
java -jar c:\Windows\apktool.jar b -c SetupWizard 
echo compilando TrainingPlan
java -jar c:\Windows\apktool.jar b -c TrainingPlan 
echo compilando UnlockMIUI
java -jar c:\Windows\apktool.jar b -c UnlockMIUI 
echo compilando WearBLE
java -jar c:\Windows\apktool.jar b -c WearBLE 
echo compilando WearCompass
java -jar c:\Windows\apktool.jar b -c WearCompass 
echo compilando WearHealth
java -jar c:\Windows\apktool.jar b -c WearHealth 
echo compilando WearHeartRate
java -jar c:\Windows\apktool.jar b -c WearHeartRate 
echo compilando WearLogger
java -jar c:\Windows\apktool.jar b -c WearLogger 
echo compilando WearSensorService
java -jar c:\Windows\apktool.jar b -c WearSensorService 
echo compilando WearServices
java -jar c:\Windows\apktool.jar b -c WearServices 
echo compilando WearSmartHome
java -jar c:\Windows\apktool.jar b -c WearSmartHome 
echo compilando WearSports
java -jar c:\Windows\apktool.jar b -c WearSports 
echo compilando XimalayaSound
java -jar c:\Windows\apktool.jar b -c XimalayaSound 
echo compilando webview
java -jar c:\Windows\apktool.jar b -c webview 

echo.
echo.

echo compilando BackupRestoreConfirmation
java -jar c:\Windows\apktool.jar b -c BackupRestoreConfirmation 
echo compilando DefaultContainerService
java -jar c:\Windows\apktool.jar b -c DefaultContainerService 
echo compilando ExternalStorageProvider
java -jar c:\Windows\apktool.jar b -c ExternalStorageProvider 
echo compilando FusedLocation
java -jar c:\Windows\apktool.jar b -c FusedLocation 
echo compilando HmAlarmClock
java -jar c:\Windows\apktool.jar b -c HmAlarmClock 
echo compilando InputDevices
java -jar c:\Windows\apktool.jar b -c InputDevices 
echo compilando ManagedProvisioning
java -jar c:\Windows\apktool.jar b -c ManagedProvisioning 
echo compilando MediaProvider
java -jar c:\Windows\apktool.jar b -c MediaProvider 
echo compilando OtaWatch
java -jar c:\Windows\apktool.jar b -c OtaWatch 
echo compilando ProxyHandler
java -jar c:\Windows\apktool.jar b -c ProxyHandler 
echo compilando SettingsProvider
java -jar c:\Windows\apktool.jar b -c SettingsProvider 
echo compilando SharedStorageBackup
java -jar c:\Windows\apktool.jar b -c SharedStorageBackup 
echo compilando Shell
java -jar c:\Windows\apktool.jar b -c Shell 
echo compilando SystemUI
java -jar c:\Windows\apktool.jar b -c SystemUI 
echo compilando WearAirPlaneMode
java -jar c:\Windows\apktool.jar b -c WearAirPlaneMode 
echo compilando WearLauncher
java -jar c:\Windows\apktool.jar b -c WearLauncher 
echo compilando WearSettings
java -jar c:\Windows\apktool.jar b -c WearSettings 
echo compilando WifiUploadData
java -jar c:\Windows\apktool.jar b -c WifiUploadData 


echo A compilação acabou!!!

pause

exit