﻿:: Backup de apks para amazfit pace
@echo off

echo AVISO LEGAL:
echo EU NAO SOU RESPONSAVEL POR QUALQUER PROBLEMA QUE VOCE VENHA A TER, COMO BRICKAR, 
echo PERDA DE DADOS, BOMBAS NUCLEARES OU QUALQUER COISA DO GENERO, FACA POR SUA CONTA!!!
echo.
echo mas estamos ae pra tirar duvidas =P (caiorrs@gmail.com)
echo.

echo BACKUP AUTOMATIZADO DE APKS PACEfied

echo Autor: Caio Roberto - @caiorrs - caiorrs@gmail.com
echo.
echo VOCE PRECISA TER O ADB INSTALADO NO SISTEMA!!!
echo se voce nao tiver, procure por 15 second adb installer no google e instale marcando "y" em todas opções

echo TENHA PELO MENOS 80%% de BATERIA NO AMAZFIT PARA CONTINUAR
echo Essa eh uma medida de seguranca!!!
echo.
echo CONECTE APENAS O AMAZFIT AO PC!!!

echo.
echo !!! ATENCAO !!! TODOS OS APKs DEVEM ESTAR NA MESMA PASTA QUE ESTE SCRIPT !!!

echo.
echo.
pause



echo Criando backup de apps instalados atualmente no PACE
echo.
echo.

mkdir App_Orig

cd App_Orig

:: BACKUP DE TODOS OS APPS DA PACEFIED

echo Backup de apps do usuario

echo Fazendo backup de AdvertiseManager
	adb pull  	/system/app/AdvertiseManager/AdvertiseManager.apk
echo Fazendo backup de Alipay
	adb pull  	/system/app/Alipay/Alipay.apk
echo Fazendo backup de AmazfitWeather
	adb pull  	/system/app/AmazfitWeather/AmazfitWeather.apk
echo Fazendo backup de Bluetooth
	adb pull  	/system/app/Bluetooth/Bluetooth.apk
echo Fazendo backup de CaptivePortalLogin
	adb pull    /system/app/CaptivePortalLogin/CaptivePortalLogin.apk
echo Fazendo backup de CertInstaller
	adb pull  	/system/app/CertInstaller/CertInstaller.apk
echo Fazendo backup de ChargingUI
	adb pull  	/system/app/ChargingUI/ChargingUI.apk
echo Fazendo backup de HmLab
	adb pull  	/system/app/HmLab/HmLab.apk
echo Fazendo backup de HmMediaPlayer
	adb pull  	/system/app/HmMediaPlayer/HmMediaPlayer.apk
echo Fazendo backup de HmTvHelper
	adb pull  	/system/app/HmTvHelper/HmTvHelper.apk
echo Fazendo backup de HmVoice
	adb pull  	/system/app/HmVoice/HmVoice.apk
echo Fazendo backup de HuamiIME
	adb pull  	/system/app/HuamiIME/HuamiIME.apk
echo Fazendo backup de HuamiSelfTest
	adb pull  	/system/app/HuamiSelfTest/HuamiSelfTest.apk
echo Fazendo backup de HuamiWatchFaces
	adb pull  	/system/app/HuamiWatchFaces/HuamiWatchFaces.apk
echo Fazendo backup de KeyChain
	adb pull  	/system/app/KeyChain/KeyChain.apk
echo Fazendo backup de MyWatch
	adb pull  	/system/app/MyWatch/MyWatch.apk
echo Fazendo backup de PackageInstaller
	adb pull 	/system/app/PackageInstaller/PackageInstaller.apk
echo Fazendo backup de SensorList
	adb pull  	/system/app/SensorList/SensorList.apk
echo Fazendo backup de SetupWizard
	adb pull 	/system/app/SetupWizard/SetupWizard.apk 
echo Fazendo backup de TrainingPlan
	adb pull  	/system/app/TrainingPlan/TrainingPlan.apk
echo Fazendo backup de UnlockMIUI
	adb pull  	/system/app/UnlockMIUI/UnlockMIUI.apk
echo Fazendo backup de WearBLE
	adb pull  	/system/app/WearBLE/WearBLE.apk
echo Fazendo backup de WearCompass
	adb pull  	/system/app/WearCompass/WearCompass.apk
echo Fazendo backup de WearHealth
	adb pull  	/system/app/WearHealth/WearHealth.apk
echo Fazendo backup de WearHeartRate
	adb pull  	/system/app/WearHeartRate/WearHeartRate.apk
echo Fazendo backup de WearLogger
	adb pull  	/system/app/WearLogger/WearLogger.apk
echo Fazendo backup de WearSensorService
	adb pull 	/system/app/WearSensorService/WearSensorService.apk
echo Fazendo backup de WearServices
	adb pull  	/system/app/WearServices/WearServices.apk
echo Fazendo backup de WearSmartHome
	adb pull  	/system/app/WearSmartHome/WearSmartHome.apk
echo Fazendo backup de WearSports
	adb pull  	/system/app/WearSports/WearSports.apk
echo Fazendo backup de webview
	adb pull  	/system/app/webview/webview.apk
echo Fazendo backup de WeeklySports
	adb pull  	/system/app/WeeklySports/WeeklySports.apk
echo Fazendo backup de XimalayaSound
	adb pull  	/system/app/XimalayaSound/XimalayaSound.apk

echo Backup de apps do sistema
echo Fazendo backup de BackupRestoreConfirmation
	adb pull  	/system/priv-app/BackupRestoreConfirmation/BackupRestoreConfirmation.apk
echo Fazendo backup de CertInstaller
	adb pull  	/system/priv-app/CertInstaller/CertInstaller.apk
echo Fazendo backup de DefaultContainerService
	adb pull 	/system/priv-app/DefaultContainerService/DefaultContainerService.apk
echo Fazendo backup de ExternalStorageProvider
	adb pull 	/system/priv-app/ExternalStorageProvider/ExternalStorageProvider.apk
echo Fazendo backup de FusedLocation
	adb pull  	/system/priv-app/FusedLocation/FusedLocation.apk
echo Fazendo backup de HmAlarmClock
	adb pull  	/system/priv-app/HmAlarmClock/HmAlarmClock.apk
echo Fazendo backup de InputDevices
	adb pull  	/system/priv-app/InputDevices/InputDevices.apk
echo Fazendo backup de ManagedProvisioning
	adb pull 	/system/priv-app/ManagedProvisioning/ManagedProvisioning.apk
echo Fazendo backup de MediaProvider
	adb pull  	/system/priv-app/MediaProvider/MediaProvider.apk
echo Fazendo backup de OtaWatch
	adb pull  	/system/priv-app/OtaWatch/OtaWatch.apk
echo Fazendo backup de ProxyHandler
	adb pull  	/system/priv-app/ProxyHandler/ProxyHandler.apk
echo Fazendo backup de SettingsProvider
	adb pull  	/system/priv-app/SettingsProvider/SettingsProvider.apk
echo Fazendo backup de SharedStorageBackup
	adb pull 	/system/priv-app/SharedStorageBackup/SharedStorageBackup.apk
echo Fazendo backup de Shell
	adb pull  	/system/priv-app/Shell/Shell.apk
echo Fazendo backup de SystemUI
	adb pull  	/system/priv-app/SystemUI/SystemUI.apk
echo Fazendo backup de WearAirPlaneMode
	adb pull 	/system/priv-app/WearAirPlaneMode/WearAirPlaneMode.apk
echo Fazendo backup de WearLauncher
	adb pull  	/system/priv-app/WearLauncher/WearLauncher.apk
echo Fazendo backup de WearSettings
	adb pull  	/system/priv-app/WearSettings/WearSettings.apk
echo Fazendo backup de WifiUploadData
	adb pull 	/system/priv-app/WifiUploadData/WifiUploadData.apk

:: FIM DO BACKUP
cd ..

echo BACKUP CONCLUIDO

echo DESCONECTE O AMAZFIT!!!
echo SE VOCE QUER EXECUTAR O INSTALADOR, DESCONECTE O AMAZFIT E CONECTE-O DE NOVO PARA PODER INSTALAR

echo.

echo Se voce gostou do trabalho e deseja fazer uma doacao expontanea
echo Banco Caixa Economica Federal
echo Nome: Caio Roberto Ramos da Silva
echo Agencia: 1765
echo Conta: 00034978-4 Operação 013 (Poupanca)
echo CPF: 020.469.780-80

echo MUITO OBRIGADO
echo.

echo Por favor, se gostou do trabalho, me de feedback
echo.
echo Me ajude em https://gitlab.com/caiorrs/PACEfied_AmazFit
echo.

:FIM
pause

exit